using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Compas : MonoBehaviour
{
    private int _BPM;
    [SerializeField]
    SpriteRenderer compasSprite;
    [SerializeField]
    Transform line;
    [SerializeField]
    public CompasController compasController;
    public Vector3 init, end;
    private Vector3 second, third, quarth;
    private float size;
    private bool isPlaying;
    int countCompas;
    float lastTimeSecond, actualTimeSecond;
    Vector3[] vecsPos = new Vector3[5];
    public CompasNotes compasNotes;
    void Awake()
    {
        compasNotes = GetComponent<CompasNotes>();
        compasController = GameObject.FindObjectOfType<CompasController>();
        Init();
    }

    public float getSpriteHeight() {
        return compasSprite.bounds.extents.y*2;
    }
    public float getSpriteWidth() {
        return compasSprite.bounds.extents.x*2;
    }

    public int getBPM() {
        return _BPM;
    }

    private void Init() {
        compasController.textCount.text = "1";
        countCompas=0;
        _BPM = compasController.BPM;
        size = compasSprite.bounds.extents.x*2;
        Vector3 pos = compasSprite.bounds.center;
        pos.x = pos.x + compasSprite.bounds.extents.x;
        end = pos;
        pos.x = pos.x - (size);
        init = pos;
        line.transform.position = init;
        pos = compasSprite.bounds.center;
        pos.x = pos.x - compasSprite.bounds.extents.x;
        float quarter = compasSprite.bounds.extents.x/4.0f;
        pos.x = pos.x + quarter*2;
        second = pos;
        pos.x = pos.x + quarter*2;
        third = pos;
        pos.x = pos.x + quarter*2;
        quarth = pos;
//        print(compasSprite.bounds.center);
        vecsPos[0] = init;
        vecsPos[1] = second;
        vecsPos[2] = third;
        vecsPos[3] = quarth;
        vecsPos[4] = end;
        line.gameObject.SetActive(false);        
    }

    private void Update() {
        if(!isPlaying)return;
        if(compasController.BPM != _BPM) {
            //PlayStop();
            _BPM = compasController.BPM;
        }
        CompasCount();
    }

    void CompasCount() {
        double lineF = Math.Round(line.transform.position.x,2);
        double vecPosF = Math.Round(vecsPos[countCompas].x,2);
        if(vecPosF<lineF) {
            countCompas++;
            if(countCompas>4) {
                PlayStop();
                countCompas=1;
                compasController.NextCompas();
            }
            compasController.textCount.text = countCompas.ToString();
        }
    }

    private void FixedUpdate() {
        if(!isPlaying)return;
        line.transform.localPosition = line.transform.localPosition + new Vector3(BPMToBPS()*Time.deltaTime, 0, 0);       
    }
    
    public float BPMToBPS(){
        return _BPM/60.000f/(4.000f-size);
    }

    public float BPSReverse() {
        return 60.000f/_BPM;
    }

    public void PlayStop() {
        Init();
        isPlaying = !isPlaying;
        //line.gameObject.SetActive(isPlaying);
        if(isPlaying){
            PlayNotes();
        }
    }

    void PlayNotes() {
        compasController.audioNote.PlayCompas(this);
    }

}
