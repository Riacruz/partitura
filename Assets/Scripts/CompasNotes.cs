using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompasNotes : MonoBehaviour
{
    Compas compas;
    
    Vector3 posCompasActual;
    bool isFirstNote;
    public List<Note> notesAdded = new List<Note>();
    float countBlacks;
    
    void Awake()
    {
        compas = GetComponent<Compas>();
        isFirstNote=true;
    }

    private void Start() {
        Init(compas.compasController.selectedScale);
    }

    private void Init(List<string> notes) {
        posCompasActual = compas.init;    
        while(countBlacks<compas.compasController.blacksForCompas)
        {
            int rand = Random.Range(0,notes.Count);
            Note n = InstantiateNote(notes[rand]);
            countBlacks += n.duration;
        }    
        print(countBlacks);
    }

    private Note InstantiateNote(string noteName) {
        int rand = Random.Range(0,compas.compasController.notes.Length);
        if(countBlacks+compas.compasController.notes[rand].duration>4) {
            rand = 2;
        }
        Note note = Instantiate<Note>(compas.compasController.notes[rand], 
            compas.init, Quaternion.identity, compas.compasController.noteParent.transform);
        MoveNoteToPosition(note, noteName);    

        if(!note.nameFigure.Contains("Silencio"))note.nameNote = noteName;
        notesAdded.Add(note);
        compas.compasController.allNotes.Add(note);
        return note;
    }

    private void MoveNoteToPosition(Note note, string noteName) {
        float middleY = compas.getSpriteHeight()/2;
        posCompasActual.y = compas.init.y;
        posCompasActual.y -= middleY;
        float distX = compas.getSpriteWidth()/4;
        float redistX = compas.getSpriteWidth()/8;
        if(isFirstNote) {
            posCompasActual.x += distX-redistX; 
            isFirstNote=false;
        } else posCompasActual.x += distX*compas.compasController.allNotes[compas.compasController.allNotes.Count-1].duration-(redistX/4); 
                
        if(note.isFixedPositionY) {
            posCompasActual.y = posCompasActual.y+note.fixedY;
        }
        else posCompasActual.y += NoteNameToDistY(noteName);
        note.transform.position = posCompasActual;
    }

    private float NoteNameToDistY(string noteName) {
        float distY = compas.getSpriteHeight()/12;
        return distY*(compas.compasController.noteNames.IndexOf(noteName));
    }
}
