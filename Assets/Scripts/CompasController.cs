using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class CompasController : MonoBehaviour
{
    public int BPM;
    [SerializeField]
    Compas compasPrefab;
    [SerializeField]
    Transform initPosition;
    List<Compas> compases = new List<Compas>();
    public TextMeshProUGUI textCount;
    int activeCompas;
    public AudioNote audioNote;
    public Note[] notes;
    public float blacksForCompas;
    public List<Note> allNotes = new List<Note>();
    public GameObject noteParent;
    public List<string> noteNames = new List<string>() {"do","re", "mi","fa","sol","la","si"};
    public List<string> noteArmonic = new List<string>() {"re","mi","sol","si", "la"};
    public List<string> notePentatonic = new List<string>() {"do","re","mi","sol", "la"};
    [SerializeField]
    private TMP_InputField inputField;
    [SerializeField]
    private TMP_InputField inputFieldBPM;

    public List<string> selectedScale = new List<string>();

    void Awake()
    {
        noteParent = new GameObject();
        audioNote=GameObject.FindObjectOfType<AudioNote>();
        selectedScale = noteNames;
        Init();
    }

    public void Init() {
        compases = new List<Compas>();
        allNotes = new List<Note>();
        noteParent.name = "NoteParent";
        CreateCompases(int.Parse(inputField.text));        
    }

    void CreateCompases(int count) {
        if(count<=7) {
            CreateFirstLine(count-1);
            return;
        }
        else {
            CreateFirstLine();
            count=count-8;
            int index = count/8;
            for (int i = 0; i < index; i++)
            {
                CreateCompasesGroup();
            }
            CreateCompasesGroup((count%8)-1);
        }
    }

    void CreateFirstLine(int count=7) {
        Compas compas = Instantiate(compasPrefab, initPosition.position, Quaternion.identity,noteParent.transform);
        compases.Add(compas);
        activeCompas=0;
        Vector3 pos = compas.transform.position;
        for(int i=0;i<count;i++) {            
            pos.x = pos.x + compas.GetComponentInChildren<SpriteRenderer>().bounds.extents.x*2;
            compases.Add(Instantiate(compasPrefab, pos, Quaternion.identity, noteParent.transform));
        }
    }

    void CreateCompasesGroup(int count=7) {
        float y = compases[compases.Count-1].transform.position.y-1.5f;
        Vector3 posInit = initPosition.position;
        posInit.y=y;
        Compas compas = Instantiate(compasPrefab, posInit, Quaternion.identity,noteParent.transform);
        compases.Add(compas);
        Vector3 pos = compas.transform.position;
        for(int i=0;i<count;i++) {            
            pos.x = pos.x + compas.GetComponentInChildren<SpriteRenderer>().bounds.extents.x*2;
            compases.Add(Instantiate(compasPrefab, pos, Quaternion.identity,noteParent.transform));
        }
    }

    public void SetBPM() {
        BPM = int.Parse(inputFieldBPM.text);
    }

    public void PlayStop() {
            audioNote.isStoped=!audioNote.isStoped;
            compases[activeCompas].PlayStop();            
            if(audioNote.isStoped)audioNote.Stop();
    }

    public void NextCompas() {
        activeCompas++;
        if(activeCompas>=compases.Count) {            
            activeCompas=0;
        }
        compases[activeCompas].PlayStop();
    }
}
