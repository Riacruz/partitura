using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DropDowController : MonoBehaviour
{
    TMP_Dropdown dropdown;
    CompasController compasController;
    void Awake()
    {
        compasController = GameObject.FindObjectOfType<CompasController>();
        dropdown=GetComponent<TMP_Dropdown>();
    }

    public void SelectDropDown() {
        if(compasController.audioNote.IsPlaying())compasController.PlayStop();
        Destroy(compasController.noteParent);
        compasController.noteParent=new GameObject();
        switch (dropdown.value)
        {            
            case 0:            
            compasController.selectedScale = compasController.noteNames;            
            break;
            case 1:
            compasController.selectedScale = compasController.noteArmonic;
            break;
            case 2:
            compasController.selectedScale = compasController.notePentatonic;
            break;            
        }
        compasController.Init();
        print(dropdown.value);
    }
}
