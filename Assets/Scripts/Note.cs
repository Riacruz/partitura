using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    public string nameFigure;
    public string nameNote;
    public float duration;
    public bool isFixedPositionY;
    public float fixedY;
    public Animator animator;
    void Awake()
    {
        animator = GetComponent<Animator>();
    }
}
