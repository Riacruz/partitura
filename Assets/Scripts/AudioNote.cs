
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNote : MonoBehaviour
{
    
    public bool isStoped;
    AudioSource audioSource;
    CompasController compasController;
    List<string> noteNames = new List<string>() {"do","re", "mi","fa","sol","la","si"};
    List<float> pitchNotes = new List<float>() {1, 1.12f, 1.25f, 1.35f, 1.5f, 1.6f, 1.85f};
    private bool isPlaying;
    void Awake()
    {
        isStoped=true;
        compasController = GameObject.FindObjectOfType<CompasController>();
        audioSource=GetComponent<AudioSource>();
    }

    public void Play(float pitch) {
        audioSource.pitch=pitch;
        audioSource.Play();
    }

    public void Stop() {
        isPlaying=false;
        audioSource.Stop();
    }

    public void PlayCompas(Compas compas) {
        isPlaying=true;
        StartCoroutine(iePlayNotes(compas.compasNotes.notesAdded.ToArray(), compas.BPSReverse()));
    }

    public void PlaySound() {
        audioSource.Play();
    }

    public bool IsPlaying() {
        return isPlaying;
    }
    
    IEnumerator iePlayNotes(Note[] notes, float bps) {
        print("BPS: "+bps);
        foreach (Note n in notes) {
            int index = noteNames.IndexOf(n.nameNote);
            float pitch = 1;
            if(index>0)
                pitch = pitchNotes[index];
            if(!n.nameFigure.Contains("Silencio") && !isStoped)
            {
                if(n.animator)n.animator.SetBool("isMove", true);
                Play(pitch);
            }                
            else audioSource.Stop();
            yield return new WaitForSecondsRealtime(bps*n.duration*1.0000f); 
            if(n.animator)n.animator.SetBool("isMove", false);           
        }
    }
}
